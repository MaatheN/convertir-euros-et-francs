//
//  main.c
//  OC_C
//
//  Created by Mathieu Neveu on 15/09/2020.
//  Copyright © 2020 Mathieu Neveu. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
double convertir(char nature, double somme);
int boolUserAnswerListener(void);


int main(int argc, const char * argv[]) {
    char nature=0;
    double somme=0,resultat=0;
    int restart=0;

    printf("Pour convertir en francs, tapez 'f'.\nPour convertir en euros, tapez 'e'.\nRéponse:");
    scanf("%c", &nature);

    printf("Indiquez la somme à convertir:\n");
    scanf("%lf", &somme);

    resultat=convertir(nature, somme);

    if(nature=='e'){
        printf("%.2f Francs valent %.2f€.\n\n",somme,resultat);
    }else if(nature=='f'){
        printf("%.2f€ valent %.2f Francs.\n",somme,resultat);
    }
    printf("Voulez-vous convertir une autre somme ?");
    restart=boolUserAnswerListener();
    if (restart) {
        main(0, 0);
    }else{
        return 0;
    }
}

double convertir(char nature, double somme){
    double ratio = 6.55957;
    double resultat=0.0;
    //1 euro = 6,55957 francs.
    switch (nature) {
        case 'e':
            resultat=somme/ratio;
            return resultat;
        case 'f':
            resultat=somme*ratio;
            return resultat;
        default:
            printf("/!\\ Veuillez répondre par e ou f. /!\\\n");
            return -1;
    }
}

// BOOLUSERANSWERLISTENER()
// return a translation of a user 'yes or not' answer by a boolean value (or -1 error)
// So the user can answer in English or French :
// -> 1 : y / Y / Yes / YES / yes / O / o / oui / OUI / carriage return
// -> 0 : n / N / No / NO / no / Non / NON
// -> -1 : other
int boolUserAnswerListener(){
    char boolUserAnswer[1] = {'\0','\0'};
    scanf(" %1c", boolUserAnswer);
    int boolStatus=-1;
    if (boolUserAnswer[0] == 'o' || boolUserAnswer[0] == 'O' || boolUserAnswer[0] == 'y' || boolUserAnswer[0] == 'Y') {
        return boolStatus = 1;
    }else if (boolUserAnswer[0] == 'n' || boolUserAnswer[0] == 'N'){
        return boolStatus = 0;
    }else
        return boolStatus;
}
